<?php
/*
 * This file is part of TechG Sf3 utility Bundle project
 *
 * (c) Roberto Beccaceci <roberto.beccaceci@techgardeners.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace TechG\Bundle\UtilityBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class BaseGeoExtendedEntity extends BaseExtendedEntity
{

  /**
   * @var string $lat
   */
  protected $lat;

  /**
   * @var string $lon
   *
   */
  protected $lon;

  /**
   * @var \DateTime $geoChanged
   *
   */
  protected $geoChanged;

  /**
   * @var string $geoChangedBy
   *
   */
  protected $geoChangedBy;


  public function __construct()
  {
  }


  /**
   * Set lat
   *
   * @param string $lat
   *
   * @return Shop
   */
  public function setLat($lat)
  {
    $this->lat = $lat;

    return $this;
  }

  /**
   * Get lat
   *
   * @return string
   */
  public function getLat()
  {
    return $this->lat;
  }

  /**
   * Set lon
   *
   * @param string $lon
   *
   * @return Shop
   */
  public function setLon($lon)
  {
    $this->lon = $lon;

    return $this;
  }

  /**
   * Get lon
   *
   * @return string
   */
  public function getLon()
  {
    return $this->lon;
  }

  /**
   * Set geoChanged
   *
   * @param \DateTime $geoChanged
   *
   * @return Shop
   */
  public function setGeoChanged($geoChanged)
  {
    $this->geoChanged = $geoChanged;

    return $this;
  }

  /**
   * Get geoChanged
   *
   * @return \DateTime
   */
  public function getGeoChanged()
  {
    return $this->geoChanged;
  }

  /**
   * Set geoChangedBy
   *
   * @param string $geoChangedBy
   *
   * @return Shop
   */
  public function setGeoChangedBy($geoChangedBy)
  {
    $this->geoChangedBy = $geoChangedBy;

    return $this;
  }

  /**
   * Get geoChangedBy
   *
   * @return string
   */
  public function getGeoChangedBy()
  {
    return $this->geoChangedBy;
  }

}