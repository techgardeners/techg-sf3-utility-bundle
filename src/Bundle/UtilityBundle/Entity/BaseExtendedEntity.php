<?php
/*
 * This file is part of TechG Sf3 utility Bundle project
 *
 * (c) Roberto Beccaceci <roberto.beccaceci@techgardeners.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace TechG\Bundle\UtilityBundle\Entity;

class BaseExtendedEntity extends BaseEntity
{

  /**
   * @var boolean $enabled
   *
   */
  protected $enabled = false;

  /**
   * @var \DateTime $created
   *
   */
  protected $created;

  /**
   * @var \DateTime $updated
   *
   */
  protected $updated;

  /**
   * @var string $createdBy
   *
   */
  protected $createdBy;

  /**
   * @var string $updatedBy
   *
   */
  protected $updatedBy;


  public function __construct()
  {
  }


  /**
   * Set enabled
   *
   * @param boolean $enabled
   *
   * @return Shop
   */
  public function setEnabled($enabled)
  {
    $this->enabled = $enabled;

    return $this;
  }

  /**
   * Get enabled
   *
   * @return boolean
   */
  public function getEnabled()
  {
    return $this->enabled;
  }

  /**
   * Set created
   *
   * @param \DateTime $created
   *
   * @return Shop
   */
  public function setCreated($created)
  {
    $this->created = $created;

    return $this;
  }

  /**
   * Get created
   *
   * @return \DateTime
   */
  public function getCreated()
  {
    return $this->created;
  }

  /**
   * Set updated
   *
   * @param \DateTime $updated
   *
   * @return Shop
   */
  public function setUpdated($updated)
  {
    $this->updated = $updated;

    return $this;
  }

  /**
   * Get updated
   *
   * @return \DateTime
   */
  public function getUpdated()
  {
    return $this->updated;
  }

  /**
   * Set createdBy
   *
   * @param string $createdBy
   *
   * @return Shop
   */
  public function setCreatedBy($createdBy)
  {
    $this->createdBy = $createdBy;

    return $this;
  }

  /**
   * Get createdBy
   *
   * @return string
   */
  public function getCreatedBy()
  {
    return $this->createdBy;
  }

  /**
   * Set updatedBy
   *
   * @param string $updatedBy
   *
   * @return Shop
   */
  public function setUpdatedBy($updatedBy)
  {
    $this->updatedBy = $updatedBy;

    return $this;
  }

  /**
   * Get updatedBy
   *
   * @return string
   */
  public function getUpdatedBy()
  {
    return $this->updatedBy;
  }

}