<?php
/*
 * This file is part of TechG Sf3 utility Bundle project
 *
 * (c) Roberto Beccaceci <roberto.beccaceci@techgardeners.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace TechG\Bundle\UtilityBundle\Entity;

class BaseEntity
{
  public function __construct()
  {
  }
}