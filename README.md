# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Tech Gardeners - Symfony 2 Utility Bundle


### VERSIONS

TAG: 1.0.X => Sf 3.0 supports

### How do I get set up? ###

install bundle

            composer require techgardeners/sf3-utility-bundle 1.0.0.5


enable bundles:

			new FOS\RestBundle\FOSRestBundle(),
            new Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new Liip\ImagineBundle\LiipImagineBundle(),
            new Ivory\CKEditorBundle\IvoryCKEditorBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new RaulFraile\Bundle\LadybugBundle\RaulFraileLadybugBundle(),
            new Knp\Bundle\SnappyBundle\KnpSnappyBundle(),
           
            // NOTE
            //If you need to use other bundles enabled its here

insert parameters:

			
	
add routes


	_liip_imagine:
		resource: "@LiipImagineBundle/Resources/config/routing.xml"
		
				
				
Install assets

            // Already done if composer install run with no error            
            php app/console assets:install

Add configuration on config.yml

			orm:
				auto_generate_proxy_classes: "%kernel.debug%"
				naming_strategy: doctrine.orm.naming_strategy.underscore
				auto_mapping: true
		#        mappings:
		#            gedmo_translatable:
		#                type: annotation
		#                prefix: Gedmo\Translatable\Entity
		#                dir: "%kernel.root_dir%/../vendor/gedmo/doctrine-extensions/lib/Gedmo/Translatable/Entity"
		#                alias: GedmoTranslatable # (optional) it will default to the name set for the mapping
		#                is_bundle: false
		#            gedmo_translator:
		#                type: annotation
		#                prefix: Gedmo\Translator\Entity
		#                dir: "%kernel.root_dir%/../vendor/gedmo/doctrine-extensions/lib/Gedmo/Translator/Entity"
		#                alias: GedmoTranslator # (optional) it will default to the name set for the mapping
		#                is_bundle: false
		#            gedmo_loggable:
		#                type: annotation
		#                prefix: Gedmo\Loggable\Entity
		#                dir: "%kernel.root_dir%/../vendor/gedmo/doctrine-extensions/lib/Gedmo/Loggable/Entity"
		#                alias: GedmoLoggable # (optional) it will default to the name set for the mappingmapping
		#                is_bundle: false
		#            gedmo_tree:
		#                type: annotation
		#                prefix: Gedmo\Tree\Entity
		#                dir: "%kernel.root_dir%/../vendor/gedmo/doctrine-extensions/lib/Gedmo/Tree/Entity"
		#                alias: GedmoTree # (optional) it will default to the name set for the mapping
		#                is_bundle: false
		#        filters:
		#            softdeleteable:
		#                class: Gedmo\SoftDeleteable\Filter\SoftDeleteableFilter
		#                enabled: true
		   
		stof_doctrine_extensions:
			default_locale: it_IT
		#
		# https://github.com/Atlantic18/DoctrineExtensions/tree/master/doc/
		#
		#    class:
		#        tree:           MyBundle\TreeListener
		#        timestampable:  MyBundle\TimestampableListener
		#        blameable:      ~
		#        sluggable:      ~
		#        translatable:   ~
		#        loggable:       ~
		#        softdeleteable: ~
		#        uploadable:     ~
		#
		#    # Only used if you activated the Uploadable extension
		#    uploadable:
		#        # Default file path: This is one of the three ways you can configure the path for the Uploadable extension
		#        default_file_path:       %kernel.root_dir%/../web/uploads
		#
		#        # Mime type guesser class: Optional. By default, we provide an adapter for the one present in the HttpFoundation component of Symfony
		#        mime_type_guesser_class: Stof\DoctrineExtensionsBundle\Uploadable\MimeTypeGuesserAdapter
		#
		#        # Default file info class implementing FileInfoInterface: Optional. By default we provide a class which is prepared to receive an UploadedFile instance.
		#        default_file_info_class: Stof\DoctrineExtensionsBundle\Uploadable\UploadedFileInfo
		#    orm:
		#        default:
		#            tree: false
		#            timestampable: false # not needed: listeners are not enabled by default

		raul_fraile_ladybug:
			theme: modern # select the theme: base, modern or custom themes
			expanded: false # true to expand all the variables tree by default
			silenced: false # true to ignore all ladybug calls

		knp_snappy:
			temporary_folder: %kernel.cache_dir%/snappy
			pdf:
				enabled:    true
				binary:     /usr/local/bin/wkhtmltopdf #"\"C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltopdf.exe\"" for Windows users
				options:    []
			image:
				enabled:    true
				binary:     /usr/local/bin/wkhtmltoimage #"\"C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltoimage.exe\"" for Windows users
				options:    []